const express = require('express');

const router = express.Router();

const {registerUser, loginUser} = require('./authServices.js');


router.post('/register', registerUser);

router.post('/login', loginUser );


module.exports = {
  authRouter: router,
};
