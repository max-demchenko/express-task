const {User} = require('../models/User.js');
const bcrypt = require('bcryptjs');
const secretKey = process.env.SECRET_KEY;
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password || !req.body) {
    return res.status(400).send({
      message: 'Please provide username and password',
    });
  }

  const newUser = new User({
    username,
    password: await bcrypt.hash(password, 10),
    createdDate: new Date().toISOString(),
  });

  try {
    const user = await User.find({username: username});

    if (user.length === 0 ) {
      return newUser.save().then(() => {
        res.json({message: 'Success'});
      });
    }
    res.status(400).send({message: 'This user already exists '});
  } catch (err) {
    next(err);
  }
};


const loginUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password || !req.body) {
    return res.status(400).send({
      message: 'Please provide username and password',
    });
  }

  try {
    const user = await User.findOne({username: username});
    const passwordIsValid = await bcrypt.compare(password, user.password);
    if (!passwordIsValid) {
      return res.status(400).send({message: 'Wrong password'});
    }
    const payload = {
      username: user.username,
      userId: user._id,
      createdDate: user.createdDate,
    };

    const jwtToken = jwt.sign(payload, secretKey);
    res.json({message: 'Success', jwt_token: jwtToken});
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registerUser,
  loginUser,
};
