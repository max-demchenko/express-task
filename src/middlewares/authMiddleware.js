const jwt = require('jsonwebtoken');
const secretKey = process.env.SECRET_KEY;

const authMiddleware = (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1];

  if (!token) {
    return res.status(400).send({message: 'Not authorized'});
  }

  jwt.verify(token, secretKey, (err, payload) => {
    if (err) {
      return res.status(400).send({message: 'Invalid token'});
    }
    req.user = {
      username: payload.username,
      userId: payload.userId,
      createdDate: payload.createdDate,
    };
    next();
  });
};


module.exports = {
  authMiddleware,
};
