require('dotenv').config();
const PORT = process.env.PORT || 8080;

const express = require('express');
const morgan = require('morgan');
const app = express();
const mongoose = require('mongoose');
const {authRouter} = require('./authEndpoint/authRouter.js');
const {userRouter} = require('./userEndpoint/userRouter.js');
const {notesRouter} = require('./notesEndpoint/notesRouter.js');

mongoose.connect(
    'mongodb+srv://newUser:user123@myfirstcluster.ypacuef.mongodb.net/?retryWrites=true&w=majority',
);

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

app.listen(PORT);

function errorHandler(err, req, res, next) {
  console.error(err);
  switch (err.name) {
    case 'SyntaxError':
      res.status(400).send({message: 'Bad request'});
    case 'TypeError':
      res.status(400).send({message: 'Resourse not found'});
    case 'CastError':
      res.status(400).send({message: 'Wrong id'});
  }
  res.status(500).send({message: 'Server error', error: err.name});
}

app.use(errorHandler);
