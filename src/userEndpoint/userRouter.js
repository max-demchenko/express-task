const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');

const {getUserInfo, deleteUser, changePassword} = require('./userServices.js');

router.get('/me', authMiddleware, getUserInfo);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changePassword);

module.exports = {
  userRouter: router,
};
