const {User} = require('../models/User');
const bcrypt = require('bcryptjs');

const getUserInfo = (req, res, next) => {
  const username = req.user.username;
  const userId = req.user.userId;
  const createdDate = req.user.createdDate;

  res.send({
    user: {
      username: username,
      _id: userId,
      createdDate: createdDate,
    },
  });
};

const deleteUser = async (req, res, next) => {
  const userId = req.user.userId;
  try {
    const user = User.findById(userId);
    user.deleteOne().then(() => res.json({message: 'Success'}));
  } catch (err) {
    next(err);
  }
};

const changePassword = async (req, res, next) => {
  const username = req.user.username;
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;

  if (!oldPassword || !req.body || !newPassword) {
    return res.status(400).send({
      message: 'Please provide old and new passwords',
    });
  }

  try {
    const user = await User.findOne({username: username});
    const passwordIsValid = await bcrypt.compare(oldPassword, user.password);
    if (!passwordIsValid) {
      return res.status(400).send({message: 'Wrong password'});
    }
    user.password = await bcrypt.hash(newPassword, 10);
    user.save().then(() => {
      res.json({message: 'Success'});
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changePassword,
};
