const express = require('express');

const router = express.Router();

const {authMiddleware} = require('../middlewares/authMiddleware');
const {
  addNewNote,
  getUserNotes,
  getNoteById,
  updateNote,
  toggleCheckNote,
  deleteNote,
} = require('./notesServices');


router.get('/', authMiddleware, getUserNotes);

router.get('/:id', authMiddleware, getNoteById);

router.post('/', authMiddleware, addNewNote);

router.put('/:id', authMiddleware, updateNote);

router.patch('/:id', authMiddleware, toggleCheckNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
