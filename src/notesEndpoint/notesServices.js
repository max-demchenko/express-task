const {Note} = require('../models/Note.js');

const getUserNotes = async (req, res, next) => {
  const userId = req.user.userId;

  try {
    const notes = await Note.find({userId: userId});
    const count = await Note.count({userId: userId});
    res.json({
      offset: 0,
      limit: 0,
      count,
      notes,
    });
  } catch (err) {
    return next(err);
  }
};

const addNewNote = (req, res, next) => {
  const userId = req.user.userId;
  const text = req.body.text;

  if (!text || !req.body) {
    return res.status(400).send({
      message: 'Provide full information about the note',
    });
  }

  const note = new Note({
    userId,
    completed: false,
    text,
    createdDate: new Date().toISOString(),
  });

  try {
    note.save().then((note) => {
      res.json({message: 'Success', note: note});
    });
  } catch (err) {
    next(err);
  }
};

const getNoteById = async (req, res, next) => {
  try {
    const noteId = req.params.id;
    const userId = req.user.userId;
    const note = await Note.findOne({'_id': noteId, 'userId': userId});
    console.log(note, userId);
    if (!note) {
      return res.status(400).send({message: 'Unauthorized'});
    }
    res.json({note});
  } catch (err) {
    next(err);
  }
};

const updateNote = async (req, res, next) => {
  const noteId = req.params.id;
  const userId = req.user.userId;
  const newText = req.body.text;
  if (!newText || !req.body) {
    return res.status(400).send({message: 'Provide new text'});
  }

  try {
    const note = await Note.findOne({'_id': noteId, 'userId': userId});
    if (!note) {
      return res.status(400).send({message: 'Unauthorized'});
    }
    note
        .updateOne({text: newText})
        .then(() => res.json({message: 'Success'}));
  } catch (err) {
    next(err);
  }
};

const toggleCheckNote = async (req, res, next) => {
  const noteId = req.params.id;
  const userId = req.user.userId;
  try {
    const note = await Note.findOne({'_id': noteId, 'userId': userId});
    if (!note) {
      return res.status(400).send({message: 'Unauthorized'});
    }
    note
        .updateOne({completed: !note.completed})
        .then(() => res.json({message: 'Success'}));
  } catch (err) {
    next(err);
  }
};

const deleteNote = async (req, res, next) => {
  const noteId = req.params.id;
  const userId = req.user.userId;
  try {
    const note = await Note.findOne({'_id': noteId, 'userId': userId});
    if (!note) {
      return res.status(400).send({message: 'Unauthorized'});
    }
    note.deleteOne().then(() => {
      res.json({message: 'Success'});
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  addNewNote,
  getUserNotes,
  getNoteById,
  updateNote,
  toggleCheckNote,
  deleteNote,
};
