const mongoose = require('mongoose');

const noteShema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    reauired: true,
  },
});

const Note = mongoose.model('note', noteShema);

module.exports = {
  Note,
};
