const mongoose = require('mongoose');

const userShema = mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: typeof Date(),
    required: true,
  },
});

const User = mongoose.model('user', userShema);

module.exports = {
  User,
};

